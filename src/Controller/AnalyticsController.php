<?php

namespace App\Controller;


use App\Entity\Activity;
use App\MyQueryBuilder\ActivityQB;
use App\MyRepo\ActivityRepo;
use App\MyRepo\UserRepo;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AnalyticsController extends AbstractController
{
    /**
     * @Route("/report", name="report")
     */
    public function report(Request $request)
    {
        $userId = $request->get('userId');
        $area = $request->get('area');

        if (!$userId) {
            throw new \Exception('UserId is not set, aborting!');
        }

        $repo = new UserRepo($this->getDoctrine()->getManager());

        // find or create user from userid
        $user = $repo->findOrCreateUser($userId);

        // get last activity for user
        $activityQB = new ActivityQB($this->getDoctrine()->getManager());
        /** @var Activity $lastActivity */
        $lastActivities = $activityQB->findLastActivityForUser($user->getId())->getQuery()->getResult();

        // save activity info
        $activity = new Activity();
        $activity->setUser($user);
        $activity->setArea($area);
        $activity->setTimestamp(Carbon::now());
        $this->getDoctrine()->getManager()->persist($activity);
        $this->getDoctrine()->getManager()->flush();


        $duration=null;
        $lastActivity=null;
        // update last activity duration
        if (count($lastActivities)==1){
            $lastActivity=$lastActivities[0];
            $previousDT=Carbon::parse($lastActivity->getTimestamp());
            $currentDT = Carbon::now();
            $duration = $currentDT->diffInSeconds($previousDT);

            $lastActivity->setDuration($duration);
            $this->getDoctrine()->getManager()->flush();
        }

        return new JsonResponse(
            [
                'userUuid' => $userId,
                'duration'=>$duration,
                'lastActivity'=>$lastActivity?$lastActivity->getId():null,
                'currentActivityId' => $activity->getId(),

            ]
        );
    }

    /**
     * @Route("/home", name="home")
     */
    public function home()
    {
        return $this->render('analytics/home.html.twig');
    }

    /**
     * @Route("/analytics", name="analytics")
     */
    public function analytics()
    {
        return $this->render('analytics/analytics.html.twig');
    }

    /**
     * @Route("/data", name="data")
     * @param Request $request
     * @return JsonResponse
     */
    public function data(Request $request)
    {
        $startTimestamp = $request->get('start');
        $endTimestamp = $request->get('end');

        $qb = new ActivityQB($this->getDoctrine()->getManager());
        $query = $qb->findAllActivitiesBetweenTwoTimestamps($startTimestamp, $endTimestamp)
            ->getQuery();
        return new JsonResponse(
            [
                'query' => $query->getSQL(),
                'result' => $this->_formatData($query->getResult())
            ]
        );
    }

    /**
     * @param Activity[] $activities
     * @return array
     */
    private function _formatData($activities)
    {
        $arrayData = [];
        foreach ($activities as $activity) {
            $userUuid = $activity->getUser()->getUuid();
            // if array with useruuid does not exist, create it
            if (!array_key_exists($userUuid, $arrayData)) {
                $arrayData[$userUuid] = ['userUuid' => $userUuid, 'activity' => []];
            }

            // calculate time on activity
            $arrayData[$userUuid]['activity'][] = [
                'area' => $activity->getArea(),
                'timestamp' => $activity->getTimestamp(),
                'duration' => $activity->getDuration()
            ];

        }
        return $arrayData;
    }
}
