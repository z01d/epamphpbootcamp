<?php


namespace App\MyRepo;


use App\Entity\User;
use Doctrine\ORM\EntityManager;

class UserRepo
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * NewUserRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findOrCreateUser($uuid): User
    {
        // get or create user
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['uuid' => $uuid]);
        if (!$user) {
            $user = new User();
            $user->setUuid($uuid);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return $user;
    }
}