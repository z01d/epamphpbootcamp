<?php


namespace App\MyQueryBuilder;


use App\Entity\Activity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;

class ActivityQB
{
    /**
     * @var QueryBuilder
     */
    private $queryBuilder;

    /**
     * ActivityQB constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->queryBuilder = $entityManager->createQueryBuilder();
    }

    public function findAllActivitiesBetweenTwoTimestamps($start, $end): QueryBuilder
    {
        return $this->queryBuilder
            ->select('a')
            ->from(Activity::class, 'a')
            ->where("a.timestamp BETWEEN '{$start}' AND '{$end}'")
            ->orderBy('a.timestamp', 'ASC');
    }

    public function findLastActivityForUser($userId): QueryBuilder
    {
        return $this->queryBuilder
            ->select('a')
            ->from(Activity::class, 'a')
            ->where("a.user=$userId")
            ->orderBy('a.timestamp', 'DESC')
            ->setMaxResults(1);
    }
}